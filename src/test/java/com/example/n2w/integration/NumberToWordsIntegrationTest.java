package com.example.n2w.integration;

import com.example.n2w.N2WApplication;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class NumberToWordsIntegrationTest {

    private final N2WApplication app = new N2WApplication();

    @Test
    @Parameters({
            "1,         one",
            "11,        eleven",
            "21,        twenty one",
            "30,        thirty",
            "99,        ninety nine",
            "200,       two hundred",
            "625,       six hundred and twenty five",
            "704,       seven hundred and four",
            "999,       nine hundred and ninety nine",
            "1234,      one thousand two hundred and thirty four",
            "6000,      six thousand",
            "24864,     twenty four thousand eight hundred and sixty four",
            "913527,    nine hundred and thirteen thousand five hundred and twenty seven",
            "999999,    nine hundred and ninety nine thousand nine hundred and ninety nine",
            "2000000,   two million",
            "3208401,   three million two hundred and eight thousand four hundred and one",
            "56945781,  fifty six million nine hundred and forty five thousand seven hundred and eighty one",
            "999999999, nine hundred and ninety nine million nine hundred and ninety nine thousand nine hundred and ninety nine"})
    public void shouldReturnWordsForValidNumbers(int number, String words) throws Exception {
        assertThat(app.convertNumberToWords(number), is(words));
    }

    @Test
    public void shouldReturnEmptyStringForOutOfRangeInput() throws Exception {
        assertThat(app.convertNumberToWords(-1), is(""));
        assertThat(app.convertNumberToWords(999_999_999 + 1), is(""));
    }
}
