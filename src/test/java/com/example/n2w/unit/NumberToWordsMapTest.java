package com.example.n2w.unit;

import com.example.n2w.NumberToWordsMap;
import com.example.n2w.NumberToWordsMapImpl;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class NumberToWordsMapTest {

    private final NumberToWordsMap numberToWordsMap = new NumberToWordsMapImpl();

    @Test
    public void shouldReturnNullForNegativeNumber() throws Exception {
        assertNull(numberToWordsMap.getWord(-1));
    }

    @Test
    public void shouldReturnEmptyStringForZero() throws Exception {
        assertThat(numberToWordsMap.getWord(0), is(""));
    }

    @Test
    public void shouldReturnCorrectWordsForNumberLessThan100() throws Exception {
        assertThat(numberToWordsMap.getWord(14), is("fourteen"));
        assertThat(numberToWordsMap.getWord(50), is("fifty"));
        assertThat(numberToWordsMap.getWord(67), is("sixty seven"));
        assertThat(numberToWordsMap.getWord(99), is("ninety nine"));
    }

    @Test
    public void shouldReturnCorrectWordsForNumberLessThan999() throws Exception {
        assertThat(numberToWordsMap.getWord(101), is("one hundred and one"));
        assertThat(numberToWordsMap.getWord(354), is("three hundred and fifty four"));
        assertThat(numberToWordsMap.getWord(500), is("five hundred"));
        assertThat(numberToWordsMap.getWord(999), is("nine hundred and ninety nine"));
    }
}
