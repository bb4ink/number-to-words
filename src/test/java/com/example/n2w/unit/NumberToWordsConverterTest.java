package com.example.n2w.unit;

import com.example.n2w.NumberToWordsConverter;
import com.example.n2w.NumberToWordsConverterImpl;
import com.example.n2w.NumberToWordsMap;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class NumberToWordsConverterTest {

    private NumberToWordsMap map = mock(NumberToWordsMap.class);
    private final NumberToWordsConverter converter = new NumberToWordsConverterImpl(map);

    @Test
    public void shouldIgnoreZeroInput() throws Exception {
        converter.convert(0);
        verifyZeroInteractions(map);
    }

    @Test
    public void shouldIgnoreNegativeInput() throws Exception {
        converter.convert(-1);
        verifyZeroInteractions(map);
    }

    @Test
    public void shouldIgnoreInputExceeding_999_999_999() throws Exception {
        converter.convert(999_999_999 + 1);
        verifyZeroInteractions(map);
    }

    @Test
    public void shouldGenerateWordFor999() throws Exception {
        when(map.getWord(999)).thenReturn("999-in-words");

        assertThat(converter.convert(999), is("999-in-words"));

        verify(map).getWord(999);
    }

    @Test
    public void shouldGenerateWordFor999_123() throws Exception {
        when(map.getWord(999)).thenReturn("999-in-words");
        when(map.getWord(123)).thenReturn("123-in-words");

        assertThat(converter.convert(999_123), is("999-in-words thousand 123-in-words"));

        verify(map).getWord(999);
        verify(map).getWord(123);
    }

    @Test
    public void shouldGenerateWordFor987_654_321() throws Exception {
        when(map.getWord(987)).thenReturn("987-in-words");
        when(map.getWord(654)).thenReturn("654-in-words");
        when(map.getWord(321)).thenReturn("321-in-words");

        assertThat(converter.convert(987_654_321), is("987-in-words million 654-in-words thousand 321-in-words"));

        verify(map).getWord(987);
        verify(map).getWord(654);
        verify(map).getWord(321);
    }
}
