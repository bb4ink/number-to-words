package com.example.n2w;

public class N2WApplication {

    private final NumberToWordsConverter converter = new NumberToWordsConverterImpl(new NumberToWordsMapImpl());

    public String convertNumberToWords(int number) {
        return converter.convert(number);
    }
}
