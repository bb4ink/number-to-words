package com.example.n2w;

public interface NumberToWordsConverter {
    String convert(int number);
}
