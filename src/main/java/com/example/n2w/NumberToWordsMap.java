package com.example.n2w;

public interface NumberToWordsMap {
    String getWord(int number);
}
