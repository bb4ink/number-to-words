package com.example.n2w;

public class NumberToWordsConverterImpl implements NumberToWordsConverter {

    private static final char SPACE = ' ';
    private static final String THOUSAND = " thousand";
    private static final String MILLION = " million";

    private final NumberToWordsMap numberToWordsMap;

    public NumberToWordsConverterImpl(NumberToWordsMap numberToWordsMap) {
        this.numberToWordsMap = numberToWordsMap;
    }

    @Override
    public String convert(int number) {

        final String value;

        if (number <= 0 || number > 999_999_999) {
            value = "";

        } else if (number <= 999) {
            value = convertHundreds(number);

        } else if (number <= 999_999) {
            value = convertThousands(number);

        } else if (number <= 999_999_999) {
            value = convertMillion(number);

        } else {
            value = null;
        }

        return value;
    }

    private String convertHundreds(int number) {
        return numberToWordsMap.getWord(number);
    }

    private String convertThousands(int number) {

        final String thousands = numberToWordsMap.getWord(number / 1_000);
        final int remainder = number % 1_000;

        final StringBuilder words = new StringBuilder(thousands).append(THOUSAND);

        if (remainder > 0) {
            words.append(SPACE).append(numberToWordsMap.getWord(remainder));
        }

        return words.toString();
    }

    private String convertMillion(int number) {

        final String millions = numberToWordsMap.getWord(number / 1_000_000);
        final int remainder = number % 1_000_000;

        final StringBuilder words = new StringBuilder(millions).append(MILLION);

        if (remainder > 0) {
            words.append(SPACE).append(convert(remainder));
        }

        return words.toString();
    }
}
