package com.example.n2w;

import java.util.HashMap;
import java.util.Map;

public class NumberToWordsMapImpl implements NumberToWordsMap {

    private static final String HUNDRED = " hundred";
    private static final String AND = " and ";

    private final Map<Integer, String> mapping = new HashMap<>(1000);

    public NumberToWordsMapImpl() {

        fillUniqueWords();

        generateWordRepresentationForNumbers();
    }

    @Override
    public String getWord(int number) {
        return mapping.get(number);
    }

    private void fillUniqueWords() {
        mapping.put(0, "");
        mapping.put(1, "one");
        mapping.put(2, "two");
        mapping.put(3, "three");
        mapping.put(4, "four");
        mapping.put(5, "five");
        mapping.put(6, "six");
        mapping.put(7, "seven");
        mapping.put(8, "eight");
        mapping.put(9, "nine");
        mapping.put(10, "ten");
        mapping.put(11, "eleven");
        mapping.put(12, "twelve");
        mapping.put(13, "thirteen");
        mapping.put(14, "fourteen");
        mapping.put(15, "fifteen");
        mapping.put(16, "sixteen");
        mapping.put(17, "seventeen");
        mapping.put(18, "eighteen");
        mapping.put(19, "nineteen");
        //multiples of Ten
        mapping.put(20, "twenty");
        mapping.put(30, "thirty");
        mapping.put(40, "forty");
        mapping.put(50, "fifty");
        mapping.put(60, "sixty");
        mapping.put(70, "seventy");
        mapping.put(80, "eighty");
        mapping.put(90, "ninety");
        mapping.put(0, "");
    }

    private void generateWordRepresentationForNumbers() {

        for (int number = 21; number <= 999; number++) {
            //Number 30, 40 .. 90 are already in the table so skip them
            if (number < 100 && number % 10 != 0) {
                mapping.put(number, mapping.get((number / 10) * 10) + " " +  mapping.get(number % 10));
            }

            if (number >= 100) {
                final String hundreds = mapping.get(number / 100);
                final int remainder = number % 100;

                final StringBuilder words = new StringBuilder(hundreds).append(HUNDRED);

                if (remainder > 0) {
                    words.append(AND).append(mapping.get(remainder));
                }
                mapping.put(number, words.toString());
            }
        }
    }
}
