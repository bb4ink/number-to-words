This application uses Gradle for build.

"NumberToWordsIntegrationTest" is the class containing integration tests which can be used to verify the functionality provided by this application.

Please let me know if you need further details.

Thanks